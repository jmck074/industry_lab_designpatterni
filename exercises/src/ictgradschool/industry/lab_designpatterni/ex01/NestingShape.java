package ictgradschool.industry.lab_designpatterni.ex01;


import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class NestingShape extends Shape {
    public List<Shape> _children = new ArrayList<>();

    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width,height);
        for(Shape c: _children){
            //System.out.println("Something in a for loop of the children");
            c.move(fWidth,fHeight);
        }
        /*
       int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;
        if(this._parent!=null) {
            if (nextX <= 0) {
                nextX = 0;
                fDeltaX = -fDeltaX;
            } else if (nextX + fWidth >= _parent.fWidth) {
                nextX = _parent.fWidth - fWidth;
                fDeltaX = -fDeltaX;
            }

            if (nextY <= 0) {
                nextY = 0;
                fDeltaY = -fDeltaY;
            } else if (nextY + fHeight >= _parent.fHeight) {
                nextY = _parent.fHeight - fHeight;
                fDeltaY = -fDeltaY;
            }

            fX = nextX;
            fY = nextY;
        }
        */

    //        super.move(width,height);
    //}

        /*int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;

       if (nextX <= this.fX) {
            nextX = this.fX;
            fDeltaX = -fDeltaX;
        } else if (nextX + fWidth >= this.fX+ this.fWidth) {
            nextX = this.fX+this.fWidth - fWidth;
            fDeltaX = -fDeltaX;
        }

        if (nextY <= this.fY) {
            nextY = this.fY;
            fDeltaY = -fDeltaY;
        } else if (nextY + fHeight >= this.fY+this.fDeltaY) {
            nextY = this.fY+this.fDeltaY - fHeight;
            fDeltaY = -fDeltaY;
        }

        fX = nextX;
        fY = nextY;*/
    }

    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(this.fX,this.fY);
        for(Shape s:_children){


        s.paint(painter);}
        painter.translate(-this.fX,-this.fY);
    }

    public void add(Shape child) throws IllegalArgumentException {
        //Iterator<Shape> i = _children.iterator();
        System.out.println("something is calling add in NestingShape");
        if(child.getWidth() >= this.getWidth() || child.getHeight() >= this.getHeight()) {
            System.out.println("add else if statement");
            throw new IllegalArgumentException();

        }
        if (child.parent() != null){
            System.out.println("Child.parent() !=null");
            throw new IllegalArgumentException();
        }
       /*if (child.fX<this.fX || child.fY<this.fY || child.fX+child.fWidth>this.fX+this.fWidth||child.fY+child.fHeight>this.fY+this.fHeight){
            System.out.println("Child doesn't fit inside");
            throw new IllegalArgumentException();
        }*/
        else{
        _children.add(child);
        child._parent=this;

    }}

    public void remove(Shape child) {
       _children.remove(child);
        child._parent=null;

    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        //not sure what goes here yet
        return _children.get(index);
    }

    public int shapeCount() {
        //not sure what goes here yet
        return _children.size();
    }

    public int indexOf(Shape child) {
        //not sure what goes here yet
        return _children.indexOf(child);
    }

    public boolean contains(Shape child) {
        //not sure what goes here yet
        return _children.contains(child);
    }


}
