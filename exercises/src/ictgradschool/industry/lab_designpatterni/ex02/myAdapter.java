package ictgradschool.industry.lab_designpatterni.ex02;

import ictgradschool.industry.lab_designpatterni.ex01.NestingShape;
import ictgradschool.industry.lab_designpatterni.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class myAdapter implements TreeModel {
    private NestingShape _adaptee;
    private List<TreeModelListener> _treeModelListeners;

    public myAdapter(NestingShape root){
        _adaptee = root;
        _treeModelListeners = new ArrayList<TreeModelListener>();
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape) {
            NestingShape s = (NestingShape) parent;
            result = s.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape s = (Shape) parent;

        if (s instanceof NestingShape) {
            NestingShape ns = (NestingShape) s;
            result = ns.shapeCount();
        }
        return result;

    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
//Apparently this is not always necessary???
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape ns = (NestingShape) parent;
            Iterator<Shape> i = ns._children.iterator();
            boolean found = false;

            int index = 0;
            while (!found && i.hasNext()) {
                Shape current = i.next();
                if (child == current) {
                    found = true;
                    indexOfChild = index;
                } else {
                    index++;
                }
            }
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

            _treeModelListeners.remove(l);
    }
}
